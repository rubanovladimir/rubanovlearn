/* Задача 4. Пользователь вводит целое число, при вводе данных пользователем
             реализовать контроль ввода(пользователь должен иметь возможность
             ввести только целое число). Если число чётное, то к нему прибавить 2,
             а если нечётное, то 1. Вывести результат. */

package org.itstep.mathematic.operations;
import java.util.Scanner;

public class TaskFour {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите целое число:");
        while(!inn.hasNextInt()){
            System.out.println("Ошибка! Вводить нужно целое число!");
            inn.next();
        }
        int a = inn.nextInt();
        if (a%2==0){
            System.out.println("Число a = " + a + " чётное. Добавим к нему 2.");
            a+=2;
        } else if (a%2!=0){
            System.out.println("Число a = " + a + " нечётное. Добавим к нему 1.");
            a+=1;
        }
        System.out.println("В итоге получаем, a = " + a);
    }
}
