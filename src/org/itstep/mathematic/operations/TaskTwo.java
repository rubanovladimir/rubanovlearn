/* Задача 2. Составить программу, которая проверяет знание таблицы умножения
             и сообщает пользователю результат (верно или нет). */

package org.itstep.mathematic.operations;
import java.util.Scanner;

public class TaskTwo {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите целое число a:");
        int a = inn.nextInt();
        System.out.println("Введите целое число b:");
        int b = inn.nextInt();
        System.out.println("Введите результат умножения c = a*b:");
        int c = inn.nextInt();
        int d = a*b;
        if(c==d){
            System.out.println("Всё верно! a*b= " + d);
        } else {
            System.out.println("Вы ошиблись! Учите таблицу умножения!");
        }
    }
}
