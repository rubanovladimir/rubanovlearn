/* Задача 5. Напишите программу, печатающую максимальный и минимальный
             элементы непустого массива типа char. */

package org.itstep.mathematic.operations;
import java.util.Arrays;

public class TaskFive {
    public static void main(String[] args) {
        char[] charArr = {'f', 'c', 'a', 'h', 'X', 'b', 'w', 'j', 'F', 'x', 'z', 'e', 'y'};
        System.out.println("Исходный массив символов: " + Arrays.toString(charArr));

        //ищем максимальный символ
        char maxChar = ' ';
        for(int i = 0; i <charArr.length; i++){
            maxChar = charArr[i];
            for(int j = 0; j<charArr.length; j++){
                if(charArr[j]>maxChar){
                    maxChar = charArr[j];
                }
            }
        }
        System.out.println();
        System.out.println("Максимальный символ массива по ASCII равен: " + maxChar);

        //ищем минимальный символ
        char minChar = ' ';
        for(int i = 0; i <charArr.length; i++){
            minChar = charArr[i];
            for(int j = 0; j<charArr.length; j++){
                if(charArr[j]<minChar){
                    minChar = charArr[j];
                }
            }
        }
        System.out.println();
        System.out.println("Минимальный символ массива по ASCII равен: " + minChar);

    }
}
