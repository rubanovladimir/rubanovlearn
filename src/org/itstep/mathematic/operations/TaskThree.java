/* Задача 3. Напишите программу, выводящую количество нулевых элементов
             в заданном целочисленном массиве. Массив можно задать произвольным образом */

package org.itstep.mathematic.operations;
import java.util.Random;
import java.util.Arrays;

public class TaskThree {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i<array.length;i++){
            array[i]=random.nextInt(5);
        }
        System.out.println("Исходный массив: " + Arrays.toString(array));
        int temp = 0;
        for(int j=0; j<array.length; j++){
            if(array[j]==0){
                temp +=1;
            }
        }
        System.out.println();
        System.out.println("Количество нулевых элементов равно: " + temp);
    }
}
