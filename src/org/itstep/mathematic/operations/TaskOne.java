/* Задача 1. Напишите программу, которая печатает массив (задан каким угодно способом),
             затем сортирует в неубывающем порядке, затем снова печатает. */

package org.itstep.mathematic.operations;
import java.util.Random;
import java.util.Arrays;

public class TaskOne {
    public static void main(String[] args) {

        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i<array.length;i++){
            array[i]=random.nextInt(50);
        }
        System.out.println("Массив ДО сортировки" + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println();
        System.out.println("Массив ПОСЛЕ сортировки" + Arrays.toString(array));
        }
    }
