/* Задача 7. Написать программу для зеркального поворота произвольного массива (1,2,3,4 -> 4,3,2,1).
             (в качестве контроля я могу поменять в задании размерность массива и код
             должен продолжать работать)*/

package org.itstep.mathematic.operations;
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class TaskSeven {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        System.out.println("Введите размерность n массива a[n]: ");
        int n = inn.nextInt();
        int[] arr = new int[n];
        Random random = new Random();
        for (int k = 0; k<arr.length; k++){
            arr[k]=random.nextInt(20);
        }
        System.out.println("Исходный массив: " + Arrays.toString(arr));
        System.out.println(" ");

        // Легкий способ № 1 с использованием второго массива
        int[] brr = new int[arr.length];
        int j = 0;
        for(int i = (arr.length-1); i>=0; i--){
            brr[j] = arr[i];
            j++;
        }
        System.out.println("Зеркальное отражение по способу 1: " + Arrays.toString(brr));
        System.out.println();

        // Более правильный способ № 2
        int temp = 0;
        for(int i=0; i<arr.length/2; i++){
            temp = arr[i];
            arr[i] = arr[arr.length-i-1];
            arr[arr.length-i-1] = temp;
        }
        System.out.println("Зеркальное отражение по способу 2: " + Arrays.toString(arr));
    }
}
